package com.celloscope.springbootdatabase.entity;

import com.celloscope.springbootdatabase.entity.Entity;
import com.celloscope.springbootdatabase.entity.Table;

@Entity
@Table(name = "customers")
public class CustomerEntity {
	private String id;
	private String name;
    private String phoneNo;

    public CustomerEntity(String name, String phoneNo) {
        this.name = name;
        this.phoneNo = phoneNo;
    }
    
    public CustomerEntity(String id,String name, String phoneNo) {
        this.id = id;
        this.name = name;
        this.phoneNo = phoneNo;
    }

    public CustomerEntity() {
    }

    public String getName() {
        return name;
    }

    

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
}
