package com.celloscope.springbootdatabase.repository;


import org.springframework.stereotype.Repository;
import com.celloscope.springbootdatabase.entity.CustomerEntity;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity, String> {
	CustomerEntity findById(String customerId);

}
