package com.celloscope.springbootdatabase.adapter.out.persistence;

import com.celloscope.springbootdatabase.application.port.out.CustomerPersistencePort;
import com.celloscope.springbootdatabase.domain.Customer;
import com.celloscope.springbootdatabase.entity.CustomerEntity;
import com.celloscope.springbootdatabase.repository.CustomerRepository;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Service
public class CustomerPersistenceAdapter implements CustomerPersistencePort {
    //TODO: Create CustomerEntity
    //TODO: Create CustomerRepository
	
	@Autowired
	private CustomerRepository customerRepository;
	
    @Override
    public Customer get(String customerId) {
    	return  customerRepository.findById(customerId).get();
    }

    @Override
    public String save(Customer customer) {
    	CustomerEntity customerEntity = new CustomerEntity();

		String customerId = "UUID-" + UUID.randomUUID().toString().substring(4);
		customerEntity.setId(customerId);
		customerEntity.setName(customer.getName());
		customerEntity.setPhoneNo(customer.getPhoneNo());
		
		CustomerEntity newCustomerId = customerRepository.save(customerEntity);
		
    	return  newCustomerId.getId();
    }
}
